import { model, Schema} from "mongoose";
import * as mongoose from "mongoose";
var DateOnly = require('mongoose-dateonly')(mongoose);

export var RecordsSchema: Schema = new Schema({

    name: String,
    date:  DateOnly,
    present: Number
})