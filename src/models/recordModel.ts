import { Document } from "mongoose";
import { IRecord } from '../interfaces/recordInterface';

export interface IRecordModel extends IRecord, Document {
  //custom methods for your model would be defined here
}