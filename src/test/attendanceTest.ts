
import { Server } from '../server';
import * as chai from 'chai';

import chaiHttp = require('chai-http');
import 'mocha';
import { expect } from 'chai';
import mongoose = require("mongoose");
import { IRecordModel } from '../models/recordModel';
import { RecordsSchema } from '../schemas/recordsSchema';

process.env.DB_NAME = 'record_test';

chai.use(chaiHttp);


var clearDB = function(done) {
    const MONGODB_CONNECTION: string = process.env.MONGODB_HOST + process.env.DB_NAME;
    console.log(MONGODB_CONNECTION);

    mongoose.set('useCreateIndex', true);
    mongoose.set('useNewUrlParser', true);
    
    let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);

    let recordTest = connection.model<IRecordModel>("recordTest", RecordsSchema);

    recordTest.deleteMany(function () {
        connection.close(function () {
            done();
        });
    });
}

after(function (done) {
    clearDB(done);
});

before(function (done) {
    clearDB(done);
});

var app = Server.bootstrap().app;

describe('Record Attendance Request', () => {

  var path = '/v1/user/record';
  var data = {
        "name": "Fish",
        "present": 1
  }

  it('record a student attendance', async () => {
    return await chai.request(app)
    .post(path)
    .send(data)
    .then(res => {
        expect(res).to.have.status(201);
        expect(res.body.status).to.be.eql('CREATED');
        expect(res.body.data._id).exist;
        expect(res.body.data.name).to.be.eql('Fish');
        expect(res.body.data.present).to.be.eql(1);
    });
  })

})

describe('List Attendance Request', () => {

    var path1 = '/v1/user/record';
    var data1 = {
        "name": "Deola",
        "present": 1
  }

    

    var path2 = '/v1/user/date';
    var data2 = {
          "date": "2019/2/4",
          
    }

  it('record a student attendance', async () => {
    await chai.request(app)
    .post(path1)
    .send(data1)
    .then(res => {
        expect(res).to.have.status(201);
        expect(res.body.status).to.be.eql('CREATED');
        expect(res.body.data._id).exist;
        expect(res.body.data.name).to.be.eql('Deola');
        expect(res.body.data.present).to.be.eql(1);
    });
  

  
    it('record a student attendance', async () => {
      return await chai.request(app)
      .get(path2)
      .send(data2)
      .then(res => {
          expect(res).to.have.status(201);
          expect(res.body.status).to.be.eql('SUCCESS');
         
          expect(res.body.data.Present).to.be.eql(1);
          expect(res.body.data.Absent).to.be.eql(0);
      });
    })
})
  })


