import { NextFunction, Request, Response} from "express";
import { RecordDTO } from '../dto/input/recordDTO';
import { validateSync } from "class-validator";
import { BaseService } from './baseService';
import { BasicResponse } from "../dto/output/basicresponse";
import { Status } from '../dto/enums/statusenums';
import { IRecordModel } from '../models/recordModel';
var DateOnly = require('dateonly');


export class Attendance extends BaseService {

    public async studentRecord(req: Request, res: Response, next: NextFunction){
        try{
            
            let dto = new RecordDTO(req.body.name, new DateOnly(), req.body.present)
            let errors = await this.validateStudentDetails(dto, req)
            if(this.hasErrors(errors)){
                return this.sendResponse(new BasicResponse(Status.FAILED_VALIDATION),req, res)
            }
            
            await this.saveStudentRecord(dto, req, res)
            return next();

        }
        catch{ex =>{
            this.sendException(ex, new BasicResponse(Status.ERROR),req,res, next)
        }}
    }



    public async searchRecord(req: Request, res: Response, next: NextFunction){

        let record = await this.fetchAttendanceByDate(req.app.locals.record, req);
        if(record){
            this.sendResponse(new BasicResponse(Status.SUCCESS, record), req, res);

        }else{
            this.sendResponse(new BasicResponse(Status.NOT_FOUND), req, res); 
        }
        return next();
    }



    async fetchAttendanceByDate(recordModel, req) {

        var Attendance= {"Present": 0, "Absent": 0}
        await recordModel.find({"date": req.body.date }).then(result => {
           
            for(var i=0; i<result.length; i++){
                
                if(result[i].present == 1){
                    Attendance.Present += 1
                }else{
                    Attendance.Absent += 1
                }
            }
        });
        return Attendance;
    }


    async validateStudentDetails(dto: RecordDTO, req: Request) {
        let errors = validateSync(dto, { validationError: { target: false }} );
        if(this.hasErrors(errors)){
            return errors;
        }
    }

    async saveStudentRecord(dto: RecordDTO, req, res){

        let { name, date, present } = dto;
        
        let recordAttendance: IRecordModel = req.app.locals.record({name, date, present})
    
        let responseObj = null;
        await recordAttendance.save().then(result =>{
            if(!result){
                responseObj = new BasicResponse(Status.FAILED_VALIDATION);
            }else{
                responseObj = new BasicResponse(Status.CREATED, result);
            }
        }).catch(err =>{
            responseObj = new BasicResponse(Status.ERROR);
        })
        return this.sendResponse(responseObj, req, res)

    }


    async findRecordForADate(file, num) {
        var found = 0;
        await file.countDocuments({ present:  num}).then(e => {
            found = e;
        });
        return found;
 
    }
    
}