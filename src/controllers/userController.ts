import { NextFunction, Request, Response, Router } from "express";
import { Attendance } from "../services/attendanceService";




export class UserController {
    public loadRoutes(prefix: string, router: Router){
        this.initRecordAttendanceRoute(prefix, router)
        this.initSearchAttendaceRoute(prefix, router)
    }


    public initRecordAttendanceRoute(prefix: String, router: Router): any {
        
        router.post(prefix+ "/record", (req, res: Response, next: NextFunction) =>{
            new Attendance().studentRecord(req, res, next);
        })
    }


    public initSearchAttendaceRoute(prefix: String, router: Router): any {
        
        router.get(prefix+ "/date", (req, res: Response, next: NextFunction) =>{
    
            new Attendance().searchRecord(req, res, next);
        })
      }

    
    
}



