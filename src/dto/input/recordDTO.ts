import { Length, IsNotEmpty } from "class-validator";

export class RecordDTO {
    @IsNotEmpty({
        message: 'fileName is required'
    })
    @Length(1, 120 ,{
        message: 'fileName should be between 1 and 100 characters' }) 
    name: string;

    date: string;
    present: number;


    constructor(name: string, date, present: number){
        this.name = name;
        this.date =date;
        this.present = present;
    }
}