
export enum Status {
    SUCCESS,
    CREATED,
    FAILED_VALIDATION,
    ERROR,
    NO_CONTENT,
    NOT_FOUND
}