
export interface IRecord {
    name?: string;
    date?: string;
    present?: number
}